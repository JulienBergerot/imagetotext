# From images to text

## Installation

Install the library in the *requirements.txt*.

```bash
pip install -r requirements.txt
```

The ```Tesseract-OCR``` folder is required and needs to be in the same directory.

## Run the main file

```bash
python main.py
```

The translate button translates the current page while the save results button translates and saves all the images. The table checkbox enables you to perform the translation if the image contains one or several tables.

## The app

You can also simply click on the ImageToText application. For it to work, you still need the ```Tesseract-OCR``` folder in the same directory.

## The API

For now, the api is launched locally.

```bash
python api.py
```

The api contains 4 different end points : 
* /pdf (for pdf files)
* /image (for images)
* table (for pdf or images containing only tables) 
* /pdftext (for pdf containing text that could be copy paste)

To produce the results, the *test_api.py* file already got the fundementals, you only need to fill the name of file etc. and then run it.

```bash
python test_api.py
```