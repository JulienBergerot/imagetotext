import requests

# your local server
BASE = "http://127.0.0.1:5000/"

# the data to send
myobj = {"filename" : "test2.png" , "lang" : "eng","output_file": "test"}

response = requests.post(BASE + "/table", myobj)
print(response.json())