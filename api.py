from flask import Flask
from flask_restful import Api, Resource, reqparse

from main import Model

import os

app = Flask(__name__)
api = Api(app)

file_args = reqparse.RequestParser()
file_args.add_argument("filename",type=str,help="Name of the file is required",required=True)
file_args.add_argument("lang",type=str,help="Langage",default="eng")
file_args.add_argument("output_file",type=str,help="Output file",default="conversion")

# work with images (jpg or png)
class Image(Resource) :
    def post(self) :
        args = file_args.parse_args()
        filename = args["filename"]
        lang = args["lang"]
        output_file = args["output_file"]

        # check if the file is an image
        if filename[-3:] != "png" and filename[-3:] != "jpg" :
            return {"error" : "The file is not png or jpg"}, 404
        #check if it exists
        if not os.path.exists(filename) :
            return {"error" : "The file {} does not exist".format(filename)}, 404

        model = Model()
        # open the file specified
        model.current_file = [filename]
        x = model.from_pdf_to_img()
        # save the result
        res = model.save_results(output_file, lang)

        return {"results" : res}, 201

# work with pdf (with images)
class Pdf(Resource) :
    def post(self) :
        args = file_args.parse_args()
        filename = args["filename"]
        lang = args["lang"]
        output_file = args["output_file"]

        # check if the file is an image     
        if filename[-3:] != "pdf" :
            return {"error" : "The file is not pdf"}, 404
        #check if it exists
        if not os.path.exists(filename) :
            return {"error" : "The file {} does not exist".format(filename)}, 404

        model = Model()
        # open the file specified
        model.current_file = [filename]
        x = model.from_pdf_to_img()
        # save the result
        res = model.save_results(output_file, lang)

        return {"results" : res}, 201

# work with pdf (with images)
class PdfText(Resource) :
    def post(self) :
        args = file_args.parse_args()
        filename = args["filename"]
        lang = args["lang"]

        # check if the file is an image
        if filename[-3:] != "pdf" :
            return {"error" : "The file is not png or jpg"}, 404
        # check if it exists
        if not os.path.exists(filename) :
            return {"error" : "The file {} does not exist".format(filename)}, 404

        model = Model()
        res = model.work_with_text_pdf([filename])

        return {"results" : res}, 201

# working with table
class Table(Resource) :
    def post(self) :
        args = file_args.parse_args()
        filename = args["filename"]
        lang = args["lang"]
        output_file = args["output_file"]

        # check if the file is an image
        if filename[-3:] != "png" and filename[-3:] != "jpg" and filename[-3:] != "pdf":
            return {"error" : "Pdf, png and jpg are the only supported formats"}, 404
        #check if it exists
        if not os.path.exists(filename) :
            return {"error" : "The file {} does not exist".format(filename)}, 404

        model = Model()
        # open the file specified
        model.current_file = [filename]
        x = model.from_pdf_to_img()
        # save the result
        res = model.save_results(output_file, lang,method="table")

        return {"results" : res}, 201


api.add_resource(Image,"/image")
api.add_resource(Pdf,"/pdf")
api.add_resource(PdfText,"/pdftext")
api.add_resource(Table,"/table")

if __name__ == '__main__' :
    app.run(debug=True)
