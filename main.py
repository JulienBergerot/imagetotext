# -*- coding: UTF-8 -*-

import tkinter as tk
from tkinter import ttk
from tkinter import scrolledtext 
from abc import ABC, abstractmethod
import easygui
import collections
import time

from table_finder import *

from pyperclip import copy as cp

from PIL import Image, ImageTk
import os
import fitz

import pytesseract
# Windows
pytesseract.pytesseract.tesseract_cmd = os.path.join(os.getcwd(),"Tesseract-OCR/tesseract.exe") #r"C:/Program Files/Tesseract-OCR/tesseract.exe"
# Mac OS : this path may change according to your configuration
pytesseract.pytesseract.tesseract_cmd = "/opt/homebrew/Cellar/tesseract/4.1.1/bin/tesseract"
import arabic_reshaper
from bidi.algorithm import get_display

from awesometkinter.bidirender import add_bidi_support, render_text

from threading import Thread

import unicodedata as ud

latin_letters= {}

def is_latin(uchr):
    try: return latin_letters[uchr]
    except KeyError:
         return latin_letters.setdefault(uchr, 'LATIN' in ud.name(uchr))

def only_roman_chars(unistr):
    return all(is_latin(uchr)
           for uchr in unistr
           if uchr.isalpha()) # isalpha suggested by John Machin

class Model :
    idx = 0
    images = []
    document_open = False
    
    def from_pdf_to_img(self) :
        """
        Open the selected document. If pdf, create an image for each page. If png, open all the images. Else, not supported
        """
        if not os.path.isdir("tmp") :
            os.mkdir("tmp")
        
        if self.current_file[0][-3:] == "pdf" :
            self.document_open = True
            self.pdf = fitz.open(self.current_file[0]) # open current_file
            self.images_name = [] # ["./tmp/page%s.png" % (idx) for idx in range(len(self.pdf))] # convert each page of the pdf to Image file and store in this list
            for current_page in range(len(self.pdf)):
                for image in self.pdf.getPageImageList(current_page):
                    xref = image[0]
                    pix = fitz.Pixmap(self.pdf, xref)
                    if pix.n < 5:        # this is GRAY or RGB
                        pix.writePNG("./tmp/page%s.png" % (current_page))
                        self.images_name.append("./tmp/page%s.png" % (current_page))
                    else:                # CMYK: convert to RGB first
                        pix1 = fitz.Pixmap(fitz.csRGB, pix)
                        pix1.writePNG("./tmp/page%s.png" % (current_page))
                        self.images_name.append("./tmp/page%s.png" % (current_page))
            self.images = []
            for im_name in self.images_name :
                img = Image.open(im_name)
                w,h = img.size
                if w > 1024 or h > 1024 :
                    img = img.resize((1024,int(1024*h/w)),Image.ANTIALIAS)
                self.images.append(img)

            self.images_translated = [""] * len(self.images)
            self.texts = [""] * len(self.images)
            return ""
        elif sum([0 if self.current_file[i][-3:] in ["png","jpg"] or self.current_file[i][-4:] == "jpeg"  else 1 for i in range(len(self.current_file))]) == 0:
            self.document_open = True
            self.images = [Image.open(self.current_file[i]) for i in range(len(self.current_file))]
            self.images_name = self.current_file
            self.idx = 0
            self.images_translated = [""] * len(self.images)
            self.texts = [""] * len(self.images)
            return ""

        else :
            self.images = []
            self.idx = 0
            self.document_open = False
            return "The format of the file is not supported, we only support pdf, png and jpeg."

    def close_pdf(self) :
        """"
        Close the images and remove all the created files and delete all information in the class
        """
        self.images = []
        self.idx = 0
        self.document_open = False
        for img in self.images_name : 
            os.remove(img)
        self.images_name = []
        try : 
            os.removedirs("tmp")  
        except : 
            pass      
        return "There is nothing to show for now."

    def translate(self,lang : str, method='classic') :
        """
        Given the current image, return the text writen. If no image is available, create a message
        """
        if self.document_open :
            # we translate
            if method == 'classic' :
                img = self.images[self.idx]
                text = pytesseract.image_to_string(img , lang=lang)
                self.texts[self.idx] = text[:-1]
                reshaped_text = arabic_reshaper.reshape(text)    # correct its shape
                bidi_text = get_display(reshaped_text)
                self.images_translated[self.idx] = bidi_text[:-1]
                return self.texts[self.idx]
            if method == 'table' :
                img = self.images[self.idx]
                img = np.array(img) 
                # Gray scale
                try :
                    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                except : 
                    pass
                #Compute the gradient
                gradh = find_contours(img,default='hor')
                gradv = find_contours(img,default='vert')
                # Find lines
                verth = find_lines(gradh,default='hor',iterations=1)
                vertv = find_lines(gradv,default='vert',iterations=1)
                # Confirm these are lines
                lines_exth,linesh = access_lines(verth,default='hor')
                lines_extv,linesv = access_lines(vertv,default='vert')
                # Delete close lines
                clean_linesh = clean_lines(lines_exth,default='hor')
                clean_linesv = clean_lines(lines_extv,default='vert')
                # Get col and line indexes
                line_idx, col_idx = col_line_indices(clean_linesh,clean_linesv)
                # Create the boxes
                boxes, position = find_boxes(clean_linesh,clean_linesv,line_idx,col_idx)
                boxes = find_empty_spaces(boxes,position,line_idx,col_idx)
                # compute the total text
                total_text = {}
                threads = []
                for idx in range(len(boxes)) :
                    threads.append(Thread(target=self.translate_box,args=(idx,boxes,img,lang,total_text,)))
                for t in threads :
                    t.start()
                threads[-1].join()
                for idx in range(len(boxes)) : # the one that are not put after the threading
                    if str(idx) not in total_text.keys() :
                        self.translate_box(idx,boxes,img,lang,total_text)
                total_text = collections.OrderedDict(sorted(total_text.items(),key = lambda t : int(t[0])))
                total_text = list(total_text.values())
                '''for idx in range(len(boxes)) :
                    text = self.translate_box(idx,boxes,img,lang)
                    total_text.append(text)'''
                # compute the text to display
                text = display_text(total_text,clean_linesv,clean_linesh,position)
                self.texts[self.idx] = text
                reshaped_text = arabic_reshaper.reshape(text)    # correct its shape
                bidi_text = get_display(reshaped_text)
                self.images_translated[self.idx] = bidi_text[:-1]
                return self.texts[self.idx]
        return "No documents found opened."

    def translate_box(self,idx : int, boxes : list, img : np.array, lang : str, total_text : dict) :
        box = boxes[idx]
        try :
            place = img[box[0][0]-1:box[1][0],box[0][1]:box[1][1]]
            text = pytesseract.image_to_string(place , lang=lang)
            if number_digits(text) > 1 :
                text = pytesseract.image_to_string(place , lang='eng')
            total_text[str(idx)] = text
        except SystemError :
            total_text[str(idx)] = " "
        return idx

    def single_trans(self, i : int, lang : str, method : str) :
        """
        Translate the image with index i, with the lang specified
        """
        img = self.images[i]
        if method == 'classic' : 
            text = pytesseract.image_to_string(img , lang=lang)
            self.texts[i] = text[:-1]
            reshaped_text = arabic_reshaper.reshape(text)    # correct its shape
            bidi_text = get_display(reshaped_text)
            self.images_translated[i] = bidi_text[:-1]
        if method == 'table' : 
            img = np.array(img) 
            # Gray scale*
            try :
                img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            except : 
                pass
            #Compute the gradient
            gradh = find_contours(img,default='hor')
            gradv = find_contours(img,default='vert')
            # Find lines
            verth = find_lines(gradh,default='hor',iterations=1)
            vertv = find_lines(gradv,default='vert',iterations=1)
            # Confirm these are lines
            lines_exth,linesh = access_lines(verth,default='hor')
            lines_extv,linesv = access_lines(vertv,default='vert')
            # Delete close lines
            clean_linesh = clean_lines(lines_exth,default='hor')
            clean_linesv = clean_lines(lines_extv,default='vert')
            # Ordering the lines
            orderh = order_lines(clean_linesh,default='hor')
            orderv = order_lines(clean_linesv,default='vert')
            line_idx, col_idx = col_line_indices(orderh,orderv)
            # Create the boxes
            boxes, position = find_boxes(clean_linesh,clean_linesv,line_idx,col_idx)
            boxes = find_empty_spaces(boxes,position,line_idx,col_idx)
            # compute the total text
            total_text = {}
            threads = []
            for idx in range(len(boxes)) :
                threads.append(Thread(target=self.translate_box,args=(idx,boxes,img,lang,total_text,)))
            for t in threads :
                t.start()
            threads[-1].join()
            for idx in range(len(boxes)) : # the one that are not put after the threading
                if str(idx) not in total_text.keys() :
                    self.translate_box(idx,boxes,img,lang,total_text)
            total_text = collections.OrderedDict(sorted(total_text.items(),key = lambda t : int(t[0])))
            total_text = list(total_text.values())
            '''for idx in range(len(boxes)) :
                text = self.translate_box(idx,boxes,img,lang)
                total_text.append(text)'''
            # compute the text to display
            text = display_text(total_text,clean_linesv,clean_linesh,position)
            self.texts[self.idx] = text
            reshaped_text = arabic_reshaper.reshape(text)    # correct its shape
            bidi_text = get_display(reshaped_text)
            self.images_translated[i] = bidi_text[:-1]

    def save_results(self,filename : str, lang : str,  method : str = 'classic') :
        """
        Translate all the images and save the results in the filename file
        """
        threads = []
        if self.document_open :
            for i in range(len(self.images)) :
                threads.append(Thread(target=self.single_trans,args=(i, lang,method,)))
                """if self.images_translated[i] == "" :
                    img = self.images[i]
                    text = pytesseract.image_to_string(img , lang='ara')
                    reshaped_text = arabic_reshaper.reshape(text)    # correct its shape
                    bidi_text = get_display(reshaped_text)
                    self.images_translated[self.idx] = bidi_text[:-1]"""
            for t in threads:
                t.start()
            threads[-1].join()
            for i,text in enumerate(self.texts) : # be sure none wenr through the threading
                if text == "" :
                    self.single_trans(i, lang, method)
            self.counted_texts = ['Page {} \n'.format(i+1) + text for i,text in enumerate(self.texts)]
            total_text = "\n\n".join(self.counted_texts)
            with open("{}.txt".format(filename),"w",encoding="utf-8") as f :
                f.write(total_text)
            return "The results are saved in the {}.txt file.".format(filename)
        return "No documents are opened for translation."

    def copy(self) :
        """"
        Save the arabic text display on paperclip
        """
        try : 
            cp(self.texts[self.idx])
        except : 
            pass

    def work_with_text_pdf(self,file_name) :
        if file_name[0][-3:] == "pdf" :
            doc = fitz.open(file_name[0])
            text = ""
            for idx,page in enumerate(doc):
                text_page = page.getText()
                if only_roman_chars(text_page) : # if arabic and not classical letters
                    text_page = arabic_reshaper.reshape(text_page)    # correct its shape
                    text_page = get_display(text_page)
                text =  text + "Page {} : \n".format(idx) + text_page
            
            with open(file_name[0].replace(".pdf",".txt"),"w",encoding="utf-8") as f :
                f.write(text)
            return "Text pdf document saved in {}.".format(file_name[0].replace(".pdf",".txt").split("\\")[-1])
        else :
            return "The only extension supported for this button if pdf."

class View(ABC) :
    @abstractmethod
    def setUp(self,controler):
        pass

    @abstractmethod
    def start_main_loop() :
        pass

class MyView(View) :
    def setUp(self,controller):
        """
        Set up the view
        """

        # global features
        self.root = tk.Tk()
        self.root.geometry("1400x1000")
        self.root.title("Image to arabic text")

        # Image loader
        self.image_frame = tk.LabelFrame(self.root,text="Image",width=500,height=600)
        self.image_frame.grid_propagate(False) 
        self.image_frame.grid(row=0,column=0)  
        self.open_image = tk.Button(self.image_frame,text="Open pdf",width=9,command=controller.find_file)
        self.open_image.place(x=210, y=550)
        self.previous = tk.Button(self.image_frame,text="<-",command=controller.previous_image)
        self.previous.place(x=170, y=550)
        self.next = tk.Button(self.image_frame,text="->",command=controller.next_image)
        self.next.place(x=297, y=550)
        self.close = tk.Button(self.image_frame,text="Close pdf",command=controller.close_pdf)
        self.close.place(x=400, y=550)

        # Text result
        self.text_frame = tk.LabelFrame(self.root,text="Text obtained",width=900,height=600)
        self.text_frame.grid_propagate(False) 
        self.text_frame.grid(row=0,column=1) 
        # self.value_text = tk.StringVar()
        # self.text = tk.Label(self.text_frame,width=35,font=("Helevetica",14,"bold"),textvariable=self.value_text)
        # self.value_text.set("There is nothing to show for now.")
        # Horizontal (x) Scroll bar
        # self.text = scrolledtext.ScrolledText(self.text_frame, width=80)

        self.text = tk.Text(self.text_frame, bd=2, width=100,height=30, relief=tk.RAISED)
        self.txscroll = tk.Scrollbar(self.text_frame, orient=tk.VERTICAL,
                                     command=self.text.yview)
        self.tyscroll = tk.Scrollbar(self.text_frame, orient=tk.HORIZONTAL,
                                     command=self.text.xview)
        self.txscroll.grid(row=0, rowspan=2, column=2, sticky='ns')
        self.tyscroll.grid(row=2,column=0,columnspan=2, sticky='ew')
        self.text.configure(yscroll=self.txscroll.set)
        self.text.configure(xscroll=self.tyscroll.set)
        self.text.grid(row=0, rowspan=2, column=0, columnspan=2, padx=(15,0),
                            pady=(15,0), sticky='nsew')
        #Configure the scrollbars
        self.update_translation("There is nothing to show for now.")

        
        self.copy = tk.Button(self.text_frame,text="Save to clipboard",command=controller.copy)
        self.copy.place(x=225, y=550)

        # Translation
        self.t_frame = tk.LabelFrame(self.root,text="Convert",width=1400,height=200)
        self.t_frame.grid_propagate(False) 
        self.t_frame.grid(row=1,columnspan=2)  
        self.open_image = tk.Button(self.t_frame,text="Convert text pdf",width=13,command=controller.pdf_text_trans)
        self.open_image.place(x=5, y=5)
        self.langage = tk.Label(self.t_frame,text="Langage :")
        self.langage.place(x=365,y= 5)
        self.lang = tk.StringVar()
        self.list_lang = ('Arabic', 'French', 'English')
        self.listeFruits = tk.ttk.Combobox(self.t_frame, textvariable = self.lang, values = self.list_lang, state = 'readonly')
        self.listeFruits.current(0)
        self.listeFruits.place(x=435,y=5)
        self.translate = tk.Button(self.t_frame,text="Convert",command=controller.translate)
        self.translate.place(x=400, y=30)
        self.table_var = tk.IntVar()
        self.table = tk.Checkbutton(self.t_frame, text='Table',variable=self.table_var, onvalue=1, offvalue=0)
        self.table.place(x=500, y=30)
        self.save_results = tk.Button(self.t_frame,text="Save results",command=controller.save_results)
        self.save_results.place(x=395, y=100)
        self.label_name = tk.Label(self.t_frame,text="Filename : ")
        self.label_name.place(x=365,y= 72)
        self.file_name = tk.Entry(self.t_frame,width=13)
        self.file_name.insert(0, "conversion")
        self.file_name.place(x=439, y=72)

    def update_translation(self,text) :
        """
        Update the value of the label in the text section
        """
        # self.value_text.set(text)
        # self.text.update()
        self.text.delete('1.0', tk.END)
        self.text.insert('1.0', text)
        self.text.update()

    def show_img(self,image : Image) :
        """
        Show the desired image in the image section
        """
        # image = Image.open(image)
        try :
            self.image_label.destroy()
        except :
            pass
        resize_image = image.resize((int(540*21/29), 540))
        img = ImageTk.PhotoImage(resize_image)
        self.image_label = tk.Label(self.image_frame,image=img)
        self.image_label.place(x=30,y=5)
        self.root.mainloop()

    def start_main_loop(self) :
        """
        Main loop for the view
        """
        self.root.mainloop()

class Controler :
    from_interface_to_ocr = {
        "Arabic" : 'ara',
        "French" : 'fra',
        "English" : 'eng'
    }
    def __init__(self,model : Model, view : View) :
        self.model = model
        self.view = view

    def next_image(self) :
        """
        Run when the next button is pressed. Display the next image
        """
        try : 
            self.model.idx = (self.model.idx + 1) % len(self.model.images)
            self.view.show_img(self.model.images[self.model.idx])
        except :
            pass
    
    def previous_image(self) :
        """
        Run when the prev button is pressed. Display the previous image
        """
        try : 
            self.model.idx = (self.model.idx - 1) % len(self.model.images)
            self.view.show_img(self.model.images[self.model.idx])
        except :
            pass

    def copy(self) :
        """
        Run when the user wants to copy to the paperclip.
        """
        self.model.copy()

    def close_pdf(self) :
        """
        Closes the pdf and reset the image
        """
        res = self.model.close_pdf()
        self.view.update_translation(res)
        try : 
            self.view.image_label.destroy()
            self.view.root.mainloop()
        except : 
            pass

    def find_file(self) :
        """
        Run when the user click the open file button. The user can choose a file to open and it will be processed.
        """
        # open a file
        self.file = easygui.fileopenbox(multiple=True)  
        self.model.current_file = self.file
        self.view.update_translation("Opening the file..")
        res = self.model.from_pdf_to_img()
        self.view.update_translation("File opened.")
        if res == "" :
            self.view.show_img(self.model.images[self.model.idx])
        else : 
            self.view.update_translation(res)
            try :  
                self.view.image_label.destroy()
            except : 
                pass

    def save_results(self) :
        """
        Run when the linked button is clicked. Translate all the images and store the results in the specified file
        """
        self.view.update_translation("Saving the results..")
        check_box = self.view.table_var.get()
        method = 'table' if check_box == 1 else 'classic'
        lang = self.from_interface_to_ocr[self.view.lang.get()]
        res = self.model.save_results(self.view.file_name.get().replace(" ","").replace("/","_").replace("'","").replace('"',"").replace("\\","").replace(".","_").replace(",","_"),lang,method=method)
        self.view.update_translation(res)

    def translate(self) :
        """
        Translate the current image and display the result
        """
        check_box = self.view.table_var.get()
        method = 'table' if check_box == 1 else 'classic'
        lang = self.from_interface_to_ocr[self.view.lang.get()]
        text = self.model.translate(lang,method=method)
        self.view.update_translation(text)

    def pdf_text_trans(self) :
        file = easygui.fileopenbox(multiple=True)  
        self.view.update_translation("Converting {}..".format(file[0].split("\\")[-1]))
        res = self.model.work_with_text_pdf(file)
        self.view.update_translation(res)

    def start(self) :
        """
        Start the program main loop
        """
        self.view.setUp(self)
        self.view.start_main_loop()


if __name__ == '__main__':
    c = Controler(Model(),MyView())
    c.start()