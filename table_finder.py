import cv2
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import os

"""
Given a certain image, the algorithm is able to find the position of the different boxes present in tables.
"""


def find_contours(img,default='vert') :
    """
    Compute the gradient along the default axis
    """
    w,h = img.shape
    grad = np.zeros((w,h))
    for x in range(w) :
        for y in range(h) :
            if default == 'vert' :
                try :
                    val = abs(int(img[x,y-1]) - int(img[x,y+1]))
                    grad[x,y] = 255 if val > 30 else 0
                except :
                    grad[x,y] = 0
            elif default == 'hor' :
                try :
                    val = abs(int(img[x-1,y]) - int(img[x+1,y]))
                    grad[x,y] = 255 if val > 30 else 0
                except :
                    grad[x,y] = 0
    return grad

def find_lines(grad,default='vert',iterations=2):
    """
    Find lines along the default axis
    """
    w,h = grad.shape
    vert = np.zeros((w,h))
    acc = [5,1] if default=='vert' else [1,5]
    for _ in range(iterations) :
        vert = np.zeros((w,h))
        for x in range(0,w,acc[0]) :
            for y in range(0,h,acc[1]) :
                count = 0 # number of pixels along this line that are indeed possibly a line
                if default == 'hor' :
                    count = np.sum(grad[x,max(y-10,0):min(y+10,h)])
                    if count > 15*255 :
                        vert[x,max(y,0):min(y+5,h)] = 255
                if default == 'vert' :
                    count = np.sum(grad[max(x-10,0):min(x+10,w),y])
                    if count > 15*255 :
                        vert[max(x,0):min(x+5,w),y] = 255
        grad = vert
    return vert

def access_lines(img,default='vert'):
    """
    Check if the lines avec long enough along the default axis
    """
    w,h = img.shape
    lines = np.zeros((w,h))
    lines_ext = []
    if default=='hor' :
        for x in range(w) :
            starty = -1
            endy = 0
            for y in range(h) :
                if img[x,y] == 255 and starty == -1 :
                    starty = y
                elif img[x,y] == 0 and starty != -1 and endy == 0:
                    endy = y
                    if endy - starty > int(0.1*h):
                        lines[x,starty:endy] = 255
                        lines_ext.append([(x,starty),(x,endy)])
                    starty = -1
                    endy = 0
        return lines_ext,lines           
    if default=='vert': 
        for y in range(h) :
            startx = -1
            endx = 0
            for x in range(w) :
                if img[x,y] == 255 and startx == -1 :
                    startx = x
                elif img[x,y] == 0 and startx != -1 and endx == 0:
                    endx = x
                    if endx - startx > int(0.05*w):
                        lines[startx:endx,y] = 255
                        lines_ext.append([(startx,y),(endx,y)])
                    endx = 0
                    startx = -1
        return lines_ext,lines

def clean_lines(lines,default='vert') :
    """
    Only keep one line in a small space along the default axis
    """
    clean_lines = []
    if default == 'hor' :
        for clean in lines : 
            x = clean[0][0]
            starty = clean[0][1]
            endy = clean[1][1]
            for _ in range(2) :
                for clean2 in lines :
                    if abs(clean2[0][0] - x) < 10 : 
                        if clean2[0][0] < x :
                            x = clean2[0][0]
                        # consider them as same line
                        if (clean2[0][1] >= starty and clean2[0][1] <= endy) or (clean2[1][1] >= starty and clean2[1][1] <= endy) :
                            if clean2[0][1] < starty :
                                starty = clean2[0][1]
                            elif clean2[1][1] > endy :
                                endy = clean2[1][1]                     
            line_to_add = [(x,starty),(x,endy)]
            if not line_to_add in clean_lines :
                clean_lines.append(line_to_add)
                
    if default == 'vert' :
        for clean in lines : 
            y = clean[0][1]
            startx = clean[0][0]
            endx = clean[1][0]
            for _ in range(2) :
                for clean2 in lines :
                    if abs(clean2[0][1] - y) < 10 : 
                        if clean2[0][1] < y :
                            y = clean2[0][1]
                        # consider them as same line
                        if (clean2[0][0] >= startx and clean2[0][0] <= endx) or (clean2[1][0] >= startx and clean2[1][0] <= endx) :
                            if clean2[0][0] < startx :
                                startx = clean2[0][0]
                            elif clean2[1][0] > endx :
                                endx = clean2[1][0]                     
            line_to_add = [(startx,y),(endx,y)]
            if not line_to_add in clean_lines :
                clean_lines.append(line_to_add)
    return clean_lines

def order_lines(lines,default='vert') :
    """
    Order the lines according to the default orientation
    """
    if default == 'vert' :
        ordered_lines = sorted(lines, key=lambda x: x[0][0] + x[0][1] / 3)
        return ordered_lines
    if default == 'hor' :
        ordered_lines = sorted(lines, key=lambda x: x[0][1] + x[0][0] / 3)
        return ordered_lines

def col_line_indices(orderh,orderv) :
    """
    Link a position to the column and a line
    """
    col_idx = {}
    line_idx = {}
    i = 0
    for line in orderh :
        x = line[0][0]
        add = True
        for _,value in line_idx.items() :
            if abs(x-value) < 6 :
                add = False
        if add :
            line_idx[str(i)] = x
            i+=1
    i = 0
    for line in orderv :
        x = line[0][1]
        add = True
        for _,value in col_idx.items() :
            if abs(x-value) < 6 :
                add = False
        if add :
            col_idx[str(i)] = x
            i+=1
    return line_idx, col_idx

def get_line(x,line_idx) :
    """
    A position gives a line
    """
    for key,value in line_idx.items() :
        if abs(x-value) < 6 :
            return key

def get_col(y,col_idx) :
    """
    A position gives a column
    """
    for key,value in col_idx.items() :
        if abs(y-value) < 6 :
            return key

def find_boxes(linesh,linesv,line_idx,col_idx) :
    """
    Given the lines obtained, create boxes
    """
    boxes = []
    position = []
    for line_t,line_b in zip(linesh[:-1],linesh[1:]) :
        x_t = line_t[0][0]+8 # x of the horinzontale line
        x_b = line_b[0][0]
        to_add = []
        for line_l in linesv :
            y_t = line_l[0][1] # y of the vertical line
            if line_l[0][0] < x_b and line_l[1][0] + 10 > x_b:
                to_add.append(y_t)
        to_add = sorted(to_add)
        for l,r in zip(to_add[:-1],to_add[1:]) :
            if x_t < x_b and l != r:
                boxes.append([(x_t,l+3),(x_b,r)])
                position.append({"col" : get_col(l,col_idx), "line" : get_line(x_t-8,line_idx)})
    return boxes, position

def find_empty_spaces(boxes,position,line_idx,col_idx) :
    """
    Look at the left of the table where there is no column 0, and create boxes associated with these missing spaces
    """
    zeros = {idx : [100,0] for idx in line_idx.keys()}
    
    for x in position :
        line = x["line"]
        col = x["col"]

        if int(col) < int(zeros[line][0]) :
            zeros[line][0] = col
        if int(col) > int(zeros[line][1]) :
            zeros[line][1] = col
    spaces = []
    for idx, val in zeros.items() :
        if val != [100,0] and val[0] != "0" :
            # there is some space at the left
            spaces.append((int(idx),0,int(val[0])))
    new_boxes = []
    for space in spaces : 
        added = False
        for i,box in enumerate(new_boxes) :
            if box[0][1] == space[0] and space[2] == box[1][1] : # prolongation
                added = True
                new_boxes[i][0][1] += 1
        if not added :
            new_boxes.append([[space[0],space[0]+1],[space[1],space[2]]])
    for b in new_boxes :
        already_added = 0
        line = b[0][0]
        col = 0
        for idx,x in enumerate(position) :
            if line == int(x["line"]) : # this is the line
                if col < int(x["col"]) :
                    new = [(int(line_idx[str(b[0][0])]),int(col_idx[str(b[1][0])])),(int(line_idx[str(b[0][1])]),int(col_idx[str(b[1][1])]))]
                    boxes.insert(idx+already_added,new)
                    position.insert(idx+already_added,{"col" : str(b[1][0]), "line" : str(b[0][0])})
                    already_added += 1
                    break
    return boxes

def number_digits(text) : 
    """
    Count the number of digits in the text
    """
    count = 0
    for c in text :
        if c.isdigit() :
            count += 1
    return count

def display_text(total_text,clean_linesv,clean_linesh,position) :
    """
    Display the text to look like a table
    """
    display_text = ''
    line = "0"
    for text, pos in zip(total_text,position) :
        if  line != pos["line"]  :
            display_text = display_text + "\n\n" + '-' * 30 + "\n\n"
        display_text = display_text + ' ' * 2 + text[:-1].replace("\n"," ") + ' '*2 + "||"
        line = pos["line"]
    return display_text
    

if __name__ == '__main__':
    import time
    curr_time = int(time.time())
    #read your file
    file=r'table.png'
    img = Image.open(file)
    img = np.array(img) 
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    print("Finding contours..")
    gradh = find_contours(img,default='hor')
    gradv = find_contours(img,default='vert')
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())

    print("Finding lines 1..")
    verth = find_lines(gradh,default='hor',iterations=1)
    vertv = find_lines(gradv,default='vert',iterations=1)
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())

    print("Finding lines 2..")
    lines_exth,linesh = access_lines(verth,default='hor')
    lines_extv,linesv = access_lines(vertv,default='vert')
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())

    print("Cleaning lines..")
    clean_linesh = clean_lines(lines_exth,default='hor')
    clean_linesv = clean_lines(lines_extv,default='vert')
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())

    print("Ordering lines..")
    orderh = order_lines(clean_linesh,default='hor')
    orderv = order_lines(clean_linesv,default='vert')
    line_idx, col_idx = col_line_indices(orderh,orderv)
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())

    print("Finding boxes..")
    boxes, position = find_boxes(orderh,orderv,line_idx,col_idx)
    print("It took {} seconds".format(int(time.time()) - curr_time))
    curr_time = int(time.time())
    